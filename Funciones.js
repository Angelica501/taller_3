

//Realizaremos una función anónima
(function () //Los paréntesis dan a entender que es anónima
{
	var a = 0;

    function miFunc1()
    {
		a = 5;
		alert(a);
	};

    function miFunc2()
    {
		a = 10;
		alert(a);
	};
})();



//Realizaremos una función callback
function ejemplo1(fn) 
{
    fn();
}
  
  ejemplo1(function() 
{
    console.log("hola");
}); 
 


//Realizaremos una función flecha
(a, b) => // a y b son los parámentros de la función
{
    let c = 42;
    return  a + b + c;
  

}